// 1. Check Winner Team

teams = {
    PowerRangers: {
        scores: [
            [44, 23, 71],
            [85, 54, 41]
        ]
    },
    FairyTails: {
        scores: [
            [65, 54, 49],
            [23, 34, 47]
        ]
    }
};

// 1.1. შექმენათ ფუქნცია (calcAverage), რომელიც დაითვლის 3 ქულის საშუალო არითმეტიკულს
// 1.2. გამოიყენათ ფუნქცია თითოეული გუნდის საშუალოს დასათვლელად
function calcAverage(array) {
    sum = 0;
    for (let i = 0; i < array.length; i++) {
        sum += array[i];
    }
    return sum / array.length;
}
console.log(calcAverage(teams.PowerRangers.scores[0]));
// 1.3. შექმენათ ფუნქცია რომელიც შეამოწმებს გამარჯვებულ გუნდს (ex. checkWinner),  მიიღებს ორ პარამეტრს გუნდების საშუალოს სახით და კონსოლში დალოგავს გამარჯვებულ გუნდს თავისი ქულებით (მაგ. პირველმა გუნდმა გაიმარჯვა 31 vs 15 ქულა).
// 1.4 გამოყენეთ ეს ფუნქცია რომ გამოავლინოთ გამარჯვებული გუნდი 1. PowerRangers (44, 23, 71) , FairyTails (65, 54, 49) 2. PowerRangers (85, 54, 41) , FairyTails (23, 34, 47)

function checkWinner(firstTeam, secondTeam) {
    firstTeam = calcAverage(firstTeam);
    secondTeam = calcAverage(secondTeam);

    if (firstTeam > secondTeam * 2) {
        return `first won ${firstTeam} vs ${secondTeam}`
    } else if (secondTeam > firstTeam * 2) {
        return `second won ${secondTeam} vs ${firstTeam}`
    } else if (firstTeam === secondTeam) {
        return 'equal'
    } else {
        return "no one won";
    }
}
console.log(checkWinner(teams.PowerRangers.scores[0], teams.FairyTails.scores[0]))

// 2. Tip Calculator

// 2.1. შექმენათ ფუქნცია (calcTip) რომელიც მიიღებს ანგარიშის მნიშნელობას პარამეტრად და დააბრუნებს თიფს ზემოთხსენებული წესების მიხედვით

function calcTip(cash) {
    let tip = 0;
    if (cash >= 50 && cash <= 300) {
        tip = (cash * 15) / 100
    } else {
        tip = (cash * 20) / 100
    }
    return tip;
}

console.log(calcTip(50))

// 2.2. შექმეათ მასივი სადაც შეინახავთ ანგარიშებს : 22, 295, 176, 440, 37, 105, 10, 1100, 96, 52 (აქ ლუფით შენახვა არაა აუცილებელი, ეს მოცემული დატაა და პირდაპირ შეგიძლიათ ჩაწეროთ ერეიში)
let expenses = [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52];
// 2.3. შექმენათ თიფების მასივი და რომელშიც შეინახავთ ფუნქციის მეშვეობით დათვლის თიფებს.

function getTips(arr) {
    let tips = [];
    for (let i = 0; i < arr.length; i++) {
        tips.push(calcTip(arr[i]))
    }
    return tips;
}
console.log(getTips(expenses))

// 2.4. შექმენათ მასივი სადაც შეინახავთ ჯამურ თანხას თითოეული ანგარიში + ანგარიშის შესაბამისი თიფი.

function sumMoney(money) {
    let sum = [];
    for (let i = 0; i < money.length; i++) {
        sum.push(money[i] + calcTip(money[i]))
    }
    return sum;
}
console.log(sumMoney(expenses))

// 2.6. შექმენათ ფუნქცია რომელიც მიიღებს მასივს პარამეტრად და დააბრუნებს საშუალოს, ამ ფუნქციით უნდა დავითვალოთ საშუალოდ რამდენ ჩაის ტოვებს ჩვენი ლუდოვიკო (2.3. პუნტში მიღებული მასივი) და ასევე უნდა დავითვალოთ საშუალოდ რა თანხა დახარჯა მანდ ჭამა-სმაში (2.4. პუნქტის მასივის საშუალო არითმეტიკული)
function avarageMoney(arr) {
    let sum = 0;
    let avarage = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i]
    }
    avarage = sum / arr.length
    return avarage;
}
console.log(avarageMoney(getTips(expenses)))
console.log(avarageMoney(sumMoney(expenses)))